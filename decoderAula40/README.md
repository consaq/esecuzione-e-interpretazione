# DECODIFICA OTTOFONICA AULA 40 PER MID-SIDE 
La decodifica Mid-Side prende due canali audio, il canale Mid e il canale Side, e li combina. 

```faust
import("stdfaust.lib");

// Definizione della funzione per manipolare l'audio Mid e Side
vmic(i, offset) = _*cos(rad)+_*sin(rad)
    with{
        rad = ((90*i)+offset)*ma.PI/180;
    };

// Definizione delle porte di ingresso per i segnali audio
vstin = si.bus(2),si.block(6);

// Definizione del processo principale
process = vstin <: par(i, 4, vmic(i,-45)) , par(i, 4, vmic(i,0));
```

### Documentazione:

#### Funzione `vmic(i, offset)`

La funzione `vmic` è utilizzata per manipolare l'audio Mid e Side. Prende due parametri:
- `i`: L'indice che rappresenta il canale audio.
- `offset`: L'offset per la manipolazione dell'audio.

La formula utilizzata per la manipolazione dell'audio è una trasformazione complessa che coinvolge sia il coseno che il seno dell'angolo calcolato in radianti.

#### Porte di Ingresso:

- `vstin`: Questa è la porta di ingresso per i segnali audio. Viene definita come un bus audio con due canali e un blocco con 6 canali.

#### Processo Principale:

- `process`: In questo blocco principale, il segnale audio in ingresso (`vstin`) viene manipolato utilizzando la funzione `vmic` con parametri specifici (-45 e 0) e poi inviato alla porta di uscita.

### Utilizzo:

Puoi utilizzare questo codice per manipolare e decodificare segnali audio Mid e Side per produrre un'uscita stereo. Puoi integrare questo codice in un ambiente Faust per processare segnali audio in tempo reale o puoi generare file audio utilizzando un compilatore Faust.

