#!/opt/homebrew/bin/python3.11

from pandocfilters import toJSONFilter, CodeBlock

def faust_filter(key, value, format, meta):
    if key == 'CodeBlock':
        [[ident, classes, keyvals], code] = value
        if "faust" in classes:
            return CodeBlock([ident, ["faust"], keyvals], code)
    return None

if __name__ == "__main__":
    toJSONFilter(faust_filter)