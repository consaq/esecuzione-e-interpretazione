import("stdfaust.lib");

g = hslider("feedback gain", 0.7, 0, 0.99, 0.01) : si.smoo;
del = (+ : de.delay(ma.SR*12,ma.SR*12))  ~ _*g ;

x(y) = hgroup("meters",y);

process = si.bus(4) : par(i,4,_ <: attach(_,abs : ba.linear2db : x(vbargraph("Level",-60,0)))) : par(i,4,del); 