## James Tenney (1934-2006)

James Tenney è stato un compositore e teorico musicale americano noto per il suo lavoro pionieristico nel campo della musica sperimentale e della teoria musicale contemporanea.

### Nascita e Formazione
James Tenney è nato il 10 agosto 1934 a Silver City, nel Nuovo Messico, USA. Ha studiato composizione musicale alla University of Denver, conseguendo il Bachelor of Music nel 1958, e ha proseguito gli studi alla Juilliard School of Music di New York City. Durante gli anni '50 e '60, Tenney è stato influenzato da compositori e teorici come John Cage, Edgard Varèse e Harry Partch, nonché dalle teorie acustiche di Ernst Chladni e Hermann von Helmholtz. Queste influenze hanno plasmato la sua prospettiva musicale verso l'approccio sperimentale e la ricerca sonora.

Ha insegnato presso istituzioni accademiche come il California Institute of the Arts, la York University in Canada e la University of California, San Diego, contribuendo alla formazione di nuove generazioni di compositori e teorici musicali. La sua musica dal 1961 al 1964 fu principalmente musica elettronica realizzata ai Bell Labs nel New Jersey con Max Mathews. Questo periodo rappresenta uno dei primi corpus significativi di musica composta algoritmicamente e sintetizzata al computer. Esempi includono *Analog #1 (Noise Study)* (1961) per nastro utilizzando rumore sintetizzato al computer e *Phases* (1963).

La musica di Tenney è caratterizzata dall'uso innovativo delle tecniche di composizione e delle tecnologie emergenti, lavorando con sistemi elettronici, microtonalità e forme strutturali non convenzionali. Oltre alla composizione, Tenney ha scritto numerosi articoli e saggi sulla acustica musicale, la percezione sonora e la matematica della musica, contribuendo significativamente alla teoria musicale contemporanea. Durante gli anni '60, Tenney visse a New York City o nei dintorni, dove fu attivamente coinvolto con Fluxus, il Judson Dance Theater e l'ensemble Tone Roads, che co-fondò insieme a Malcolm Goldstein e Philip Corner. Tenney mostrò un impegno eccezionale per la musica del compositore americano Charles Ives, di cui diresse molte composizioni; la sua interpretazione della *Concord Sonata* di Ives per pianoforte fu molto apprezzata. È deceduto il 24 agosto 2006 a Valencia, in California.

### Saxony
*Saxony* è una composizione di James Tenney del 1978 pensata per un sassofonista (che suonerà l'intera famiglia di sassofoni) e un sistema di tape-delay (ritardo a nastro). Tenney utilizza il tape-delay per esplorare le possibilità di ritardo e ripetizione nel contesto della performance dal vivo. Questo permette al sassofonista di interagire con le proprie registrazioni ritardate, creando effetti di densità timbrica che arricchiscono l'esperienza musicale complessiva.

## Idea Personale sul Brano

La partitura di James Tenney offre un ampio margine interpretativo attraverso una semplicità espositiva che permette una varietà di deviazioni nell'interpretazione.

Studiando la musica spettrale ed esaminando alcuni aspetti delle composizioni di Tenney, ho trovato particolare interesse nell'esplorare l'esecuzione della sua opera *Saxony*. Ciò che mi ha colpito è stato soprattutto l'uso della serie naturale come elemento formale dominante.

Nonostante possa avere delle riserve compositive e semiografiche riguardo alla modalità in cui è scritta la partitura, in particolare per l'utilizzo della notazione tradizionale mensurale e alcune indicazioni scritte (di cui parlerò in seguito), ho immediatamente trovato affascinanti alcuni aspetti musicali che contribuiscono alla semplicità della scrittura e, contemporaneamente, alla complessità del risultato sonoro del brano. Questi aspetti includono:

- L'impiego della serie naturale per creare un drone strutturale.
- L'adozione di un attacco "dal nulla" per ogni evento sonoro.
- L'utilizzo di macro comportamenti all'interno di macro sezioni (dinamica, altezze, tempi).
- La semplicità nell'integrazione dell'elettronica dal vivo.

Questi elementi combinati conferiscono a *Saxony* di James Tenney una caratterizzazione unica e un'intrigante complessità che invita a un'esplorazione più approfondita durante l'esecuzione e nell'analisi interpretativa della partitura.

### Elementi Formali Strutturali
Come accennato precedentemente, il brano è costruito sui primi 15 armonici di Mi bemolle, altezza che si ottiene nel sassofono nella posizione di Do. Dal momento che Tenney specifica che la famiglia di strumenti può variare nella realizzazione, ho scelto di lavorare con i clarinetti. Utilizzando un altro strumento mi sono domandato quale fosse il rapporto tra lo strumento (la famiglia) e le altezze reali, per comprendere se in quelle altezze o in quei registri ci fossero delle particolari condizioni spettrali e quindi se il brano necessitasse di una trascrizione. Così è stato affrontato uno studio delle altezze e della scelta delle varie posizioni. Dopo un'attenta catalogazione delle altezze reali e degli armonici è stato compreso come la scelta di Tenney fosse probabilmente legata più alla semplicità, in quanto a posizioni sul sassofono, della serie degli armonici della nota Do più che a particolari condizioni timbriche. Questi ragionamenti sono risultati comunque utili perché ci hanno dato possibilità di riflettere sulla varietà di scelta di armonici presi da diverse fondamentali per suonare una determinata altezza e tra le posizioni in cui quella data altezza si trova alla fondamentale. Così abbiamo iniziato a lavorare sulla costruzione di droni che variano la loro forma spettrale in base alla presenza o meno di date armoniche.

Osservando il brano in una veste più ampia, il brano è costituito da 9 sezioni. Vi è una struttura ad arco nel profilo delle altezze, arricchita da un accelerando e un ritardando, da un crescendo e diminuendo per registri dinamici e da una aumentazione e diminuzione della durata delle varie sezioni. Se si pone in relazione la durata delle sezioni in funzione della durata del tape delay, diviene evidente l'atteggiamento minimalista di phasing temporale utilizzato da Tenney. Il tape ha una durata di 12 secondi, le sezioni hanno una durata di \(12*10+(i*(12+12/4))\) per \(i\) da 0 a 4, dove \(i\) è il numero delle sezioni e \(12*10+((8-i)*(12+12/4))\) per \(i\) da 5 a 8.

Ultimo elemento su cui porre attenzione per le riflessioni è la scelta di usare delle note nere come fossero note di passaggio nella quinta sezione.

## Idea Realizzativa ed Esecutiva

La domanda alla base della realizzazione è stata: come avrebbe voluto realizzare il brano il compositore con la tecnologia che ho a disposizione? Questa domanda ha portato a una serie di riflessioni che hanno indirizzato l'utilizzo di due diversi feedback:

- Un feedback interno e virtuale.
- Un feedback esterno tra altoparlanti in sala e microfoni.

Immaginando il compositore a contatto con Alvin Lucier, non ho potuto esimermi dal creare un apparato che avesse come modulo di delay e aberrazione quello di *I am Sitting in a Room* di Lucier.

Questo file Faust implementa un effetto di feedback delay con un'interfaccia grafica per il controllo del guadagno del feedback e dei livelli di segnale. Ecco una descrizione dettagliata del processo:

Per ogni canale, viene visualizzato un indicatore di livello. Il segnale di ciascun canale passa attraverso un delay con feedback, il cui guadagno del feedback è controllabile tramite uno slider grafico. Il feedback crea un effetto di ritardo, il cui livello può essere monitorato tramite gli indicatori di livello grafici. Questo schema consente di controllare e applicare un effetto di delay con feedback ai segnali audio, mentre fornisce un feedback visivo dei livelli di segnale in tempo reale.

```faust
import("stdfaust.lib");

g = hslider("feedback gain", 0.7, 0, 0.99, 0.01) : si.smoo;
del = (+ : de.delay(ma.SR*12,ma.SR*12))  ~ _*g ;

x(y) = hgroup("meters",y);

process = si.bus(4) : par(i,4,_ <: attach(_,abs : ba.linear2db : x(vbargraph("Level",-60,0)))) : par(i,4,del);
```

![https://gitlab.com/consaq/esecuzione-e-interpretazione/-/blob/main/Tesina/scrittura_libera/docs/saxonyTenney-svg/process.svg?ref_type=heads](docs/saxonyTenney-svg/process.svg)

la funzione del implementa un effetto di delay con feedback utilizzando un ritardo fisso e un guadagno di feedback controllabile, integrando così un effetto sonoro dinamico e controllabile nel flusso audio generale.

![https://gitlab.com/consaq/esecuzione-e-interpretazione/-/blob/main/Tesina/scrittura_libera/docs/saxonyTenney-svg/del-0x135b924d0.svg](docs/saxonyTenney-svg/del-0x135b924d0.svg)

Come si può osservare, sono presenti 4 ingressi e 4 uscite: Tetrarec come tecnica di microfonazione e S.T.One come strumento altoparlante.

### TETRAREC
Il sistema TETRAREC si basa sulla registrazione tetraedrica, un metodo ampiamente conosciuto e sviluppato inizialmente da Michael Gerzon nel 1971. Questo metodo prevede l'uso di una configurazione microfonica tetraedrica, in cui quattro capsule microfoniche sono posizionate sulle facce di un ipotetico tetraedro molto vicine tra loro. Questa configurazione permette di catturare un campo sonoro tridimensionale, registrando le informazioni spaziali delle onde sonore che attraversano il sistema di capsule. Il principio alla base del TETRAREC non è solo quello di registrare un evento acustico, ma anche di descrivere in modo modulabile tale evento, integrando strettamente il sistema di registrazione con il sistema di diffusione. Questa tecnologia permette quindi di ottenere una rappresentazione sonora estremamente dettagliata e spazialmente accurata, utile per la composizione e l'esecuzione di musica elettroacustica avanzata.

### S.T.ONE
Il progetto S.T.ONE, acronimo di Spherical Tetrahedral ONE, nasce dall'esigenza di eseguire musica elettroacustica con caratteristiche percettive analoghe a quelle degli strumenti tradizionali. Questo diffusore avanzato consente di controllare la propagazione del suono in tutte le direzioni dello spazio, permettendo un'integrazione perfetta tra suoni acustici ed elettroacustici. La tecnologia S.T.ONE supera i limiti degli altoparlanti tradizionali, che possono controllare solo la dimensione dinamica della potenza, e introduce la possibilità di gestire anche i fattori timbrici e spaziali del suono. Questo rende possibile una fusione totale e multidimensionale tra i due tipi di suono, offrendo una nuova esperienza di ascolto. S.T.ONE permette quindi performance live in cui l'illusione di un unicum sonoro è mantenuta e rafforzata, arricchendo significativamente la composizione elettroacustica.

### Quali mic, stone, e perché
La tecnica di microfonazione è la Tetrarec.
I microfoni utilizzati sono dei Line Audio M1 omnidirezionali.

**Microfono pre-polarizzato a condensatore**
- **Figura polare:** Omnidirezionale
- **Dimensione:** 77 x 20 mm 
- **Made in Svezia (no cina)**
- **Max SPL:** 133 dB
- Richiede alimentazione phantom 12-52 Volts
- **Freq. risposta:** 20-20.000 Hz (±1 dB)
- **Impedenza:** < 100 Ohm
- **Sensibilità:** 8 mV/PA -42 dBV
- **Peso:** 30 g

![https://gitlab.com/consaq/esecuzione-e-interpretazione/-/blob/main/Tesina/scrittura_libera/docs/Omni1.jpg?ref_type=heads](docs/Omni1.jpg)![https://gitlab.com/consaq/esecuzione-e-interpretazione/-/blob/main/Tesina/scrittura_libera/docs/Omni1plot.png?ref_type=heads](docs/Omni1plot.png)
Gli altoparlanti utilizzati sono gli S.T.One di Giuseppe Silvi, altoparlanti tetraedrici.

### Messa in scena e ambiente esecutivo

La messa in scena è scelta in funzione di come è stato pensata tutta l'idea realizzativa, ma con delle modifiche date dall'impossibilità del luogo. La musicista inizialmente doveva trovarsi al centro sala, successivamente è stata spostata sul palco. L'utilizzo di S.T.One e Tetrarec permettono una ripresa e riproduzione del campo acustico come sorgente sferica.

![https://gitlab.com/consaq/esecuzione-e-interpretazione/-/blob/main/Tesina/scrittura_libera/docs/tenney.svg?ref_type=heads](docs/tenney.svg)

L'ambiente esecut

## Bibliografia

[Giuseppe silvi, appunti. PTO](https://gitlab.com/giuseppesilvi/pto/-/blob/master/2020-PTO.c.pdf?ref_type=heads)